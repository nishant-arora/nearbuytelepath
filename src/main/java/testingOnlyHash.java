import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.ml.feature.Tokenizer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.feature.Word2Vec;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.ml.classification.DecisionTreeClassificationModel;
import org.apache.spark.ml.feature.StopWordsRemover;
import org.apache.spark.ml.classification.DecisionTreeClassifier;
import org.apache.spark.ml.classification.GBTClassificationModel;
import org.apache.spark.ml.classification.DecisionTreeClassificationModel;
import org.apache.spark.ml.feature.NGram;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.ml.feature.HashingTF;
import org.apache.spark.ml.feature.IDF;
import org.apache.spark.ml.feature.IDFModel;
import java.lang.String;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.classification.LogisticRegressionModel;
public class testingOnlyHash {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			SparkConf conf = new SparkConf().setAppName("Name").setMaster("local");
			JavaSparkContext sc = new JavaSparkContext(conf);
			SparkSession ssc = new SparkSession(sc.sc());
			SQLContext spark = ssc.sqlContext();
			
//			DecisionTreeClassificationModel dtModel = DecisionTreeClassificationModel.load("/Users/nishantarora/Downloads/hashAndW2VnewData" );
			GBTClassificationModel dtModel = GBTClassificationModel.load("/Users/nishantarora/Downloads/gbTreeLoad");

			String s = " ";
			Dataset<Row> string = spark.read().text("/Users/nishantarora/Downloads/string.txt");
//			string.show(false);
			Tokenizer tokenizer = new Tokenizer().setInputCol("value").setOutputCol("tokens");
			Dataset<Row> tokens = tokenizer.transform(string);
//			tokens.show(false);
			StopWordsRemover swr = new StopWordsRemover().setInputCol("tokens").setOutputCol("words");
			Dataset<Row> words = swr.transform(tokens);
//			words.show(false);
			NGram unigram = new NGram().setN(1).setInputCol("words").setOutputCol("unigrams");
			NGram bigram = new NGram().setN(2).setInputCol("words").setOutputCol("bigrams");
			words = unigram.transform(words);
			words = bigram.transform(words);
//			words.show(false);
			Dataset<Row> explode = words.withColumn("uni", org.apache.spark.sql.functions.explode(words.col("unigrams"))).withColumn("bi", org.apache.spark.sql.functions.explode(words.col("bigrams")));
//			explode.show(false);
			Dataset<Row> unigrams = explode.select("uni");
			Dataset<Row> bigrams = explode.select("bi");
			words = unigrams.union(bigrams).distinct();
			


			Dataset<Row> finalWords = words.groupBy("uni").agg(org.apache.spark.sql.functions.collect_list("uni").as("Words"));
			
//			int numFeatures = 10;
			HashingTF hash = HashingTF.load("/Users/nishantarora/Downloads/99hash");
			Dataset<Row> hashed = hash.transform(finalWords);
//			hashed.show(false);
			IDFModel idf = IDFModel.load("/Users/nishantarora/Downloads/99idf");
			hashed = idf.transform(hashed);
//			hashed.show(false);
//			finalWords.show(false);
			//Word2Vec word2Vec = new Word2Vec().setInputCol("Words").setOutputCol("w2v").setVectorSize(10).setMinCount(0);

//			words = w2v.select(w2v.col("Words"),w2v.col("features"));

			
			
		//	Double x = dtModel.predict((Vector) words.select("features"));
			//System.out.println(x);
			
			
			Dataset<Row> predict=dtModel.transform(hashed);
			predict.show(false);
			
			
			
			
	}

}
