import static spark.Spark.post;

import java.util.List;

import com.google.gson.Gson;

class stringQuery {

	private String query;

	public void setQuery(String s) {
		this.query = s;
	}
}

public class Telepathy {

	public static String q = null;

	public static void main(String[] args) {
		Gson gson = new Gson();

		// TODO Auto-generated method stub
		queryPredictor.configure();
		//queryPredictor function = new queryPredictor();

		post("/predictEntity", (req, res) -> {
			q = req.queryParams("q");
			stringQuery obj = new stringQuery();
			obj.setQuery(q);
			String jsonString = gson.toJson(obj);

			List<NameEntity> nameEntities = queryPredictor.stringPredictor(jsonString);
			return gson.toJson(nameEntities);
		});

	}

}
