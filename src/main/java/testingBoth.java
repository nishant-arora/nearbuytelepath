import org.apache.commons.beanutils.converters.StringConverter;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.ml.feature.Tokenizer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.feature.Word2Vec;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.ml.classification.DecisionTreeClassificationModel;
import org.apache.spark.ml.feature.StopWordsRemover;
import org.apache.spark.ml.classification.DecisionTreeClassifier;
import org.apache.spark.ml.classification.GBTClassificationModel;
import org.apache.spark.ml.classification.DecisionTreeClassificationModel;
import org.apache.spark.ml.feature.NGram;
import org.apache.spark.ml.linalg.Vector;
//import org.apache.spark.util.Vector;
import org.apache.spark.ml.linalg.DenseVector;
import org.apache.spark.ml.feature.VectorSlicer;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.ml.feature.SQLTransformer;

import scala.collection.Seq;
import scala.collection.mutable.WrappedArray;

import org.apache.spark.ml.feature.HashingTF;
import org.apache.spark.ml.feature.IDF;
import org.apache.spark.ml.feature.IDFModel;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.classification.LogisticRegressionModel;
import org.apache.spark.ml.classification.RandomForestClassificationModel;


public class testingBoth {
	
	public static String stringConvert(String s)
	{
		int l = s.length();
		return s.substring(2, l-2);
	}
	public static String stringConvert2(String s)
	{
		int l = s.length();
		return s.substring(1, l-1);
	}
	

	public static void main(String[] args) {
			
		// TODO Auto-generated method stub
			SparkConf conf = new SparkConf().setAppName("Name").setMaster("local");
			JavaSparkContext sc = new JavaSparkContext(conf);
			SparkSession ssc = new SparkSession(sc.sc());
			SQLContext spark = ssc.sqlContext();
			
//			DecisionTreeClassificationModel dtModel = DecisionTreeClassificationModel.load("/Users/nishantarora/Downloads/hashAndW2VnewData" );
//			GBTClassificationModel dtModel = GBTClassificationModel.load("/Users/nishantarora/Downloads/Final");
			RandomForestClassificationModel dtModel = RandomForestClassificationModel.load("/Users/nishantarora/Downloads/rfFinal");

			String s = " ";
			Dataset<Row> string = spark.read().text("/Users/nishantarora/Downloads/string.txt");
//			string.show(false);
			Tokenizer tokenizer = new Tokenizer().setInputCol("value").setOutputCol("tokens");
			Dataset<Row> tokens = tokenizer.transform(string);
//			tokens.show(false);
			StopWordsRemover swr = new StopWordsRemover().setInputCol("tokens").setOutputCol("words");
			Dataset<Row> words = swr.transform(tokens);
//			words.show(false);
			NGram unigram = new NGram().setN(1).setInputCol("words").setOutputCol("unigrams");
			NGram bigram = new NGram().setN(2).setInputCol("words").setOutputCol("bigrams");
			words = unigram.transform(words);
			//words.withColumn("len", functions.lit(1.0));
			words = bigram.transform(words);
//			words.show(false);
			Dataset<Row> explode = words.withColumn("bi", org.apache.spark.sql.functions.explode(words.col("bigrams"))).withColumn("uni", org.apache.spark.sql.functions.explode(words.col("unigrams")));
			explode.show(false);
			Dataset<Row> unigrams = explode.select("uni");
			Dataset<Row> bigrams = explode.select("bi");
			words = bigrams.union(unigrams).distinct();
			SQLTransformer sql = new SQLTransformer().setStatement("SELECT *, length(bi) AS Len FROM words");
			words.createOrReplaceTempView("words");
			words = sql.transform(words);
			Dataset<Row> len = words.select("bi","Len");
			len.show(false);
//			words.show(false);


			Dataset<Row> finalWords = words.groupBy("bi").agg(org.apache.spark.sql.functions.collect_list("bi").as("Words"));
			Column c = len.col("Len");
		
//			finalWords = finalWords.select("Words");
			finalWords = finalWords.as("f").join(len.as("l"), "bi").select("f.Words","l.Len");
//			finalWords = finalWords.withColumn("yo", c);
//			finalWords = finalWords.numericColumns();
//			words = words.groupBy("bi").agg(org.apache.spark.sql.functions.collect_list("bi").as("Words"));
//			words.show(false);			
//			finalWords = finalWords.select("Words",len.select("Len").as("ss"));
//			finalWords = finalWords.union(len);	
//			SQLTransformer sql = new SQLTransformer().setStatement("SELECT *, org.apache.spark.sql.functions.length(Words) AS Len FROM finalWords");
//			finalWords.createOrReplaceTempView("finalWords");
//			finalWords = sql.transform(finalWords);
//			finalWords = finalWords.withColumn("len", functions.lit(finalWords.col("Words").toString()));
//			finalWords = finalWords.wit
			finalWords = finalWords.sort(org.apache.spark.sql.functions.desc("Len"));
			finalWords.show(false);
			int numFeatures = 20;
			HashingTF hash = HashingTF.load("/Users/nishantarora/Downloads/RFhashModel");
			IDFModel idf = IDFModel.load("/Users/nishantarora/Downloads/RFidfModel");

			Dataset<Row> Final = hash.transform(finalWords);
			Final = idf.transform(Final);
			
//			hashed = hashed.select(hashed.col("Words"),hashed.col("features1").as("hashs"));
			Word2VecModel model = Word2VecModel.load("/Users/nishantarora/Downloads/rfW2VModel");
			Final = model.transform(Final);
//			Dataset<Row> w2v = model.transform(finalWords).select("Words","w2v");
//			Dataset<Row> w2vecs = w2v.select("Words","w2v");
//			Dataset<Row> union = hashed.union(w2vecs);
//			union.show(false);
			VectorAssembler assembler = new VectorAssembler()
					  .setInputCols(new String[]{"w2v", "features1"})
					  .setOutputCol("features");

			Final = assembler.transform(Final);
//			finalWords.show(false);
			//Word2Vec word2Vec = new Word2Vec().setInputCol("Words").setOutputCol("w2v").setVectorSize(10).setMinCount(0);

//			words = w2v.select(w2v.col("Words"),w2v.col("features"));

			
			
		//	Double x = dtModel.predict((Vector) words.select("features"));
			//System.out.println(x);
			
			
			Dataset<Row> predict=dtModel.transform(Final);
			predict = predict.select("Words","probability","prediction");
			predict.show(false);
			Boolean locFlag = true, mercFlag = true;
			for(Row r: predict.collectAsList()){
				Vector v= r.getAs(1);
				if(r.getAs(2).equals(0.0) && locFlag)
				{
					if(v.apply(0)>=0.7)
					{
						WrappedArray<String> wa = (WrappedArray<String>) r.get(0);
						String loc = wa.mkString(" ");
						System.out.println("Location is " + loc + " with a probability of " + v.apply(0));
						locFlag = false;
					}
				}
				else if(r.getAs(2).equals(1.0) && mercFlag)
				{
					if(v.apply(1)>=0.7)
					{
						WrappedArray<String> wa = (WrappedArray<String>) r.get(0);
						String merc = wa.mkString(" ");
						System.out.println("Merchant is " + merc + " with a probability of " + v.apply(1));
						mercFlag = false;
					}
				}
			}


	
			
			
	}

}
