
import java.io.IOException;
import java.util.ArrayList;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.feature.Tokenizer;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.ml.feature.StopWordsRemover;
import org.apache.spark.ml.feature.NGram;
import org.apache.spark.ml.feature.Word2Vec;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.catalyst.expressions.ConcatWs;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.dmg.pmml.DataType;
import org.apache.spark.ml.feature.VectorSlicer;
import org.apache.spark.sql.functions;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.classification.LogisticRegressionModel;
import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS;
import org.apache.spark.ml.classification.NaiveBayes;
import org.apache.spark.ml.classification.NaiveBayesModel;
import org.apache.spark.ml.feature.MinMaxScaler;
import org.apache.spark.ml.feature.MinMaxScalerModel;
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.feature.HashingTF;
import org.apache.spark.ml.feature.IDF;
import org.apache.spark.ml.feature.IDFModel;
import org.apache.spark.ml.classification.DecisionTreeClassifier;
import org.apache.spark.ml.classification.DecisionTreeClassificationModel;
import org.apache.spark.mllib.util.MLUtils;
import org.apache.spark.ml.feature.VectorAssembler;

public class noIDFwithDT {

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SparkConf conf = new SparkConf().setAppName("Name").setMaster("local");
		JavaSparkContext sc = new JavaSparkContext(conf);
		SparkSession ssc = new SparkSession(sc.sc());
		SQLContext spark = ssc.sqlContext();
		
		// For Location
		
		Dataset<Row> Location_training1 = spark.read().format("csv").option("header", "true").load("/Users/nishantarora/Downloads/location.csv");
		Dataset<Row> Location_training2 = spark.read().format("csv").option("header", "true").load("/Users/nishantarora/Downloads/allLocality.csv");
		Dataset<Row> Location_training3 = spark.read().format("csv").option("header", "true").load("/Users/nishantarora/Downloads/allMalls.csv");
		Location_training1 = Location_training1.select(Location_training1.col("redemptionAddress_locality_name").as("loc"));
		Location_training2 = Location_training2.select(Location_training2.col("Locations").as("loc"));
		Location_training3 = Location_training3.select(Location_training3.col("Localities").as("loc"));
		Dataset<Row> Location_training = Location_training1.union(Location_training2).union(Location_training3).distinct();
		Location_training = Location_training.filter(Location_training.col("loc").isNotNull());
//		System.out.println(Location_training.count());
//		Location_training = Location_training.union(Location_training3).un.distinct();
		Tokenizer Location_tokenizer=new Tokenizer().setInputCol("loc").setOutputCol("words");				  
		Dataset<Row> Location_output=Location_tokenizer.transform(Location_training);
		StopWordsRemover Location_remover = new StopWordsRemover().setInputCol("words").setOutputCol("words2");
		Dataset<Row> Location_afterStop = Location_remover.transform(Location_output);
		Word2Vec Location_word2Vec = new Word2Vec().setInputCol("words2").setOutputCol("w2v").setVectorSize(4).setMinCount(0);
		Word2VecModel Location_model = Location_word2Vec.fit(Location_afterStop);
		Dataset<Row> Location_w2v = Location_model.transform(Location_afterStop).select("words2","w2v");
		int numFeatures = 15;
		HashingTF Location_hashingTF = new HashingTF()
				  .setInputCol("words2")
				  .setOutputCol("rawFeatures")
				  .setNumFeatures(numFeatures);

		Dataset<Row> Location_featurizedData = Location_hashingTF.transform(Location_w2v);
				// alternatively, CountVectorizer can also be used to get term frequency vectors

//		IDF Location_idf = new IDF().setInputCol("rawFeatures").setOutputCol("features1");
//		IDFModel Location_idfModel = Location_idf.fit(Location_featurizedData);

//		Dataset<Row> Location_rescaledData = Location_idfModel.transform(Location_featurizedData);
		Dataset<Row> Location_vector = Location_featurizedData;
		VectorAssembler Location_assembler = new VectorAssembler()
				  .setInputCols(new String[]{"rawFeatures", "w2v"})
				  .setOutputCol("features");

		Location_vector = Location_assembler.transform(Location_vector);
		Location_vector = Location_vector.select("words2","features");
//		Location_vector.show(false);
		
		


		Location_vector = Location_vector.withColumn("Label", functions.lit(0.0));		
		Location_vector = Location_vector.select(Location_vector.col("words2").as("Words"),Location_vector.col("features"),Location_vector.col("Label").as("label"));

//		System.out.println(Location_vector.count());

			
		//		Location_vector.show(false);


		//For Merchant
		
		Dataset<Row> Merchant_training = spark.read().format("csv").option("header", "true").load("/Users/nishantarora/Downloads/merchants.csv").distinct();
		System.out.println(Merchant_training.count());
		Tokenizer Merchant_tokenizer=new Tokenizer().setInputCol("name").setOutputCol("words");				  
		Dataset<Row> Merchant_output=Merchant_tokenizer.transform(Merchant_training);
		StopWordsRemover Merchant_remover = new StopWordsRemover().setInputCol("words").setOutputCol("words2");
		Dataset<Row> Merchant_afterStop = Merchant_remover.transform(Merchant_output);
		Word2Vec Merchant_word2Vec = new Word2Vec().setInputCol("words2").setOutputCol("w2v").setVectorSize(4).setMinCount(0);
		Word2VecModel Merchant_model = Merchant_word2Vec.fit(Merchant_afterStop);
		Dataset<Row> Merchant_w2v = Merchant_model.transform(Merchant_afterStop).select("words2","w2v");
		
		HashingTF Merchant_hashingTF = new HashingTF()
				  .setInputCol("words2")
				  .setOutputCol("rawFeatures")
				  .setNumFeatures(numFeatures);

		Dataset<Row> Merchant_featurizedData = Merchant_hashingTF.transform(Merchant_w2v);
				// alternatively, CountVectorizer can also be used to get term frequency vectors

//		IDF Merchant_idf = new IDF().setInputCol("rawFeatures").setOutputCol("features1");
//		IDFModel Merchant_idfModel = Merchant_idf.fit(Merchant_featurizedData);

//		Dataset<Row> Merchant_rescaledData = Merchant_idfModel.transform(Merchant_featurizedData);
		Dataset<Row> Merchant_vector = Merchant_featurizedData;
		VectorAssembler Merchant_assembler = new VectorAssembler()
				  .setInputCols(new String[]{"rawFeatures", "w2v"})
				  .setOutputCol("features");

		Merchant_vector = Merchant_assembler.transform(Merchant_vector);
		Merchant_vector = Merchant_vector.select("words2","features");
 
		Merchant_vector = Merchant_vector.withColumn("Label", functions.lit(1.0));		
		Merchant_vector = Merchant_vector.select(Merchant_vector.col("words2").as("Words"),Merchant_vector.col("features"),Merchant_vector.col("Label").as("label"));
		

		double[] arr = {.2,.8};
		Dataset<Row>[] splitting = Merchant_vector.randomSplit(arr);
		Merchant_vector = splitting[0];
		double[] fifty = {0.8,0.2};
		Dataset<Row>[] Location_splitting = Location_vector.randomSplit(fifty);
		Dataset<Row> Location_trainingDF = Location_splitting[0];
		Dataset<Row> Location_testingDF  = Location_splitting[1];
		
		Dataset<Row>[] Merchant_splitting = Merchant_vector.randomSplit(fifty);
		Dataset<Row> Merchant_trainingDF = Merchant_splitting[0];
		Dataset<Row> Merchant_testingDF  = Merchant_splitting[1];
//		double[] array = {0.7,0.2,0.1};


//		Dataset<Row> mergedDF =  Location_vector.union(Merchant_vector);
		

//		Dataset<Row>[] merged_splitting = mergedDF.randomSplit(array);
		Dataset<Row> trainingDF = Location_trainingDF.union(Merchant_trainingDF);
		Dataset<Row> testingDF  = Location_testingDF.union(Merchant_testingDF);
//		Dataset<Row> validationDF = Location_validationDF.union(Merchant_validationDF);
		
		DecisionTreeClassifier dt = new DecisionTreeClassifier()
				  .setLabelCol("label")
				  .setFeaturesCol("features")
				  .setSeed(1L);
		DecisionTreeClassificationModel dtModel = dt.fit(trainingDF);


		Dataset<Row> predictions = dtModel.transform(testingDF);
//		predictions.show(false);
		MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator()
				  .setLabelCol("label")
				  .setPredictionCol("prediction")
				  .setMetricName("accuracy");
		double accuracy = evaluator.evaluate(predictions);
		
		
		
		System.out.println("Test set accuracy = " + accuracy + " count = " + predictions.filter(predictions.col("prediction").equalTo(0.0)).count() + " " + predictions.filter(predictions.col("label").equalTo(0.0)).count() + " count = " + predictions.filter(predictions.col("prediction").equalTo(1.0)).count() + " " + predictions.filter(predictions.col("label").equalTo(1.0)).count());
//		predictions.filter(predictions.col("prediction").equalTo(0.0)).count();
		
				System.out.println("Learned classification tree model:\n" + dtModel.toDebugString());
//		predictions.filter(predictions.col("label").equalTo(1.0)).show(200);

		
		
//		trainingDF.filter(trainingDF.col("label").equalTo(0.0)).show(false);

//		predictions.show(false);
//		System.out.println("Coefficients: "+ lrModel.coefficients() + " Intercept: " + lrModel.intercept());	
//		System.out.println(Location_training.count() + "" + Merchant_training.count());
//		System.out.println(mergedDF.count() + " " + trainingDF.count() + " " + testingDF.count() + " " + validationDF.count());
//		mergedDF.filter(mergedDF.col("label").equalTo(1.0)).show(250);

		try {
			dtModel.write().overwrite().save("/Users/nishantarora/Downloads/noIDF");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
